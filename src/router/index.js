import Vue from 'vue'
import VueRouter from 'vue-router'
import Recipes from '../views/Recipes.vue'
import ShoppingList from '../views/ShoppingList.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Recipes',
    component: Recipes
  },
  {
    path: '/shopping-list',
    name: 'ShoppingList',
    component: ShoppingList
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
