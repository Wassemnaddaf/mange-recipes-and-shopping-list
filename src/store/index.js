import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import recipe from './modules/recipe'
import shoppingList from './modules/shoppingList'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    user: user,
    recipe: recipe,
    shoppingList: shoppingList
  }
})
