export default {
    namespaced: true,
    state: {
        items: [],
    },
    mutations: {
        setItems:(state, items) => {
            state.items = items
        },
        addItem:(state, item) => {
            state.items.push(item)
            localStorage.setItem('shopping-list', JSON.stringify(state.items));
        },
    },
    getters: {
        getItems(state){
            return state.items
        },
        getItemsCount(state){
            return state.items.length
        }
    },
    actions: {
        addToList({ commit }, item) {
            commit('addItem', item)
        },
        initItems({ commit }) {
            let items = JSON.parse(localStorage.getItem('shopping-list'))
            // if items list is empty init the empty array
            if(!items){
                items = []
            }
            else {
                commit('setItems', items)
            }
        }
    }
}
