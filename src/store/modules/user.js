import router from '@/router/index'

export default {
    namespaced: true,
    state: {
        token: null,
        name: '',
        users: [],
        staticUser: {
            email: 'test@test.com',
            password: 'password'
        }
    },
    mutations: {
        saveSession: (state, session) => {
            state.token = session.token
            localStorage.setItem('token', state.token)
        },
        clearSession: (state) => {
            state.token = null
            localStorage.removeItem('token')
        },
        setUsers:(state, users) => {
            state.users = users
        },
        addUser:(state, user) => {
            state.users.push(user)
            localStorage.setItem('users', JSON.stringify(state.users));
        }
    },
    getters: {
        getStaticUser(state){
            return state.staticUser
        },
        getUsers(state){
            return state.users
        },
        isLoggedIn(state) {
            return state.token !== null
        }
    },
    actions: {
        register({ commit }, userInfo) {
            commit('addUser', userInfo)
        },
        login({ commit, getters }, userInfo) {
            return new Promise(function(resolve, reject) {
                // check if user exist
                let users = getters.getUsers;
                let user = users.find(e => e.email == userInfo.email && e.password == userInfo.password)
                if (user) {
                    console.log(user)
                    commit('saveSession', {token: userInfo.email})
                    resolve()
                }
                else reject()
            })
        },
        logout({ commit }) {
            commit('clearSession')
            router.push('/login')
        },
        initUsers({ commit, getters }) {
            let users = JSON.parse(localStorage.getItem('users'))
            // if users list is empty init the static user
            if(!users || users.length == 0){
                commit('addUser', getters.getStaticUser)
            }
            else {
                commit('setUsers', users)
            }
        },
        updateSession({ commit }) {
            let token = localStorage.getItem('token')
            if(token){
                commit('saveSession', {token: token})
            }
        }
    }
}
