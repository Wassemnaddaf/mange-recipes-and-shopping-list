export default {
    namespaced: true,
    state: {
        recipes: [],
    },
    mutations: {
        setRecipes:(state, recipes) => {
            state.recipes = recipes
        },
        addRecipe:(state, recipe) => {
            state.recipes.push(recipe)
            localStorage.setItem('recipes', JSON.stringify(state.recipes));
        },
        editRecipe:(state, recipe) => {
            Object.assign(state.recipes[recipe.idx], recipe.recipe);
            localStorage.setItem('recipes', JSON.stringify(state.recipes));
        },
        removeRecipe: (state, recipe) => {
            state.recipes.splice(recipe, 1);
            localStorage.setItem('recipes', JSON.stringify(state.recipes));
        }
    },
    getters: {
        getRecipes(state){
            return state.recipes
        }
    },
    actions: {
        addRecipe({ commit }, recipeInfo) {
            commit('addRecipe', recipeInfo)
        },
        editRecipe({ commit }, recipeInfo) {
            commit('editRecipe', recipeInfo)
        },
        removeRecipe({ commit }, recipe) {
            commit('removeRecipe', recipe)
        },
        initRecipes({ commit }) {
            let recipes = JSON.parse(localStorage.getItem('recipes'))
            // if recipes list is empty init the empty array
            if(!recipes){
                recipes = []
            }
            else {
                commit('setRecipes', recipes)
            }
        }
    }
}
